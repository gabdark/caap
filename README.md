Git global setup

git config --global user.name "Gabriel Gaudreau"
git config --global user.email "gabe.gaudreau@gmail.com"

Create a new repository

git clone git@gitlab.com:najomie/goudrons-laurentides.git
cd goudrons-laurentides
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin git@gitlab.com:najomie/goudrons-laurentides.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin git@gitlab.com:najomie/goudrons-laurentides.git
git push -u origin --all
git push -u origin --tags