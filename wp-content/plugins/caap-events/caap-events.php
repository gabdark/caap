<?php
/**
 * Plugin Name: CAAP - Événements
 * Plugin URI: caap.ca
 * Description: Gestion des événements
 * Version: 1.0.8
 * Author: Gabriel Gaudreau
 * Author URI: http://gabrielgaudreau.com
 * Requires at least: 4.1
 * Tested up to: 4.1
 *
 * @package NAJ
 * @category Core
 * @author Gabriel Gaudreau
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'CAAP_Events' ) ) {

	class CAAP_Events
	{
		const DEBUG = FALSE;

		/**
		 * Plugin version, match with plugin header
		 * @var string
		 */
		public $version = '1.0.0';

		/**
		 * Use the function not the variable
		 * @var string
		 */
		public $plugin_url;

		/**
		 * Use the function not the variable
		 * @var string
		 */
		public $plugin_path;

		/**
		 * Do we update the rewrite rules for a custom post type?
		 * @var boolean
		 */
		public $flush_rules = FALSE;

		/**
		 * PLUGIN STARTUP
		 */
		public function __construct(){
			// do something when we activate/deactivate the plugin
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

			$installed = get_option( __CLASS__ . '_Version', FALSE );

			if( ! $installed or version_compare($installed, $this->version, '!=') ){
				add_action( 'init', array($this, 'activate'), 9 );
				update_option( __CLASS__ . '_Version', $this->version );
			}

			$this->hooks();
		}

		/**
		 * Register the plugin's hooks
		 */
		public function hooks(){
			add_action( 'init', array($this, 'init'), 0 );
		}

		/**
		 * Runs on WordPress init hook
		 */
		public function init(){
			$this->post_types();
			$this->flush();
		}
		/**
		 * Register the post types and taxonomies
		 *
		 * @link http://fooplugins.com/generators/wordpress-custom-post-types/
		 * @link http://fooplugins.com/generators/wordpress-custom-taxonomy/ (OFFLINE)
		 * @link https://developer.wordpress.org/resource/dashicons/ for admin menu icons
		 */
		 // Register Custom Post Type
		 public function post_types() {

			 $labels = array(
				 'name'               => __( 'Catégorie', 'tga' ),
				 'singular_name'      => __( 'Catégorie', 'tga' ),
				 'menu_name'          => __( 'Catégorie', 'tga' ),
			 );

			$args = array(
				'labels'                     => $labels,
				'hierarchical'               => TRUE,
				'public'                     => TRUE,
				'show_ui'                    => TRUE,
				'show_admin_column'          => TRUE,
				'show_in_nav_menus'          => TRUE,
				'show_tagcloud'              => FALSE,
			);

			register_taxonomy( 'event_cat', array('event_cat'), $args );

			$labels = array(
				'name'                  => _x( 'Événements', 'Post Type General Name', 'tga' ),
				'singular_name'         => _x( 'Événement', 'Post Type Singular Name', 'tga' ),
				'menu_name'             => __( 'Événements', 'tga' ),
				'name_admin_bar'        => __( 'Événement', 'tga' ),
				'archives'              => __( 'Événement', 'tga' ),
				'parent_item_colon'     => __( 'Événement parent:', 'tga' ),
				'all_items'             => __( 'Tout les événements', 'tga' ),
				'add_new_item'          => __( 'Ajouter les événements', 'tga' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'tga' ),
				'set_featured_image'    => __( 'Image à la une', 'tga' ),
				'remove_featured_image' => __( 'Remove featured image', 'tga' ),
				'use_featured_image'    => __( 'Use as featured image', 'tga' ),
				'insert_into_item'      => __( 'Insert into item', 'tga' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'tga' ),
				'items_list'            => __( 'Items list', 'tga' ),
				'items_list_navigation' => __( 'Items list navigation', 'tga' ),
				'filter_items_list'     => __( 'Filter items list', 'tga' ),
			);
			$args = array(
				'label'                 => __( 'Événement', 'tga' ),
				'description'           => __( 'Événements', 'tga' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'thumbnail', 'editor' ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_icon'           	=> 'dashicons-tickets-alt',
				'menu_position'         => 5,
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'taxonomies'          => array('event_cat'),
				'rewrite'             => array(
					'slug'       => 'evenements',
					'with_front' => FALSE,
					'feeds'      => TRUE,
					'pages'      => TRUE
				),
				'capability_type'       => 'page',
			);
			register_post_type( 'event', $args );
		 }

		/**
		 * Refresh rewrite rules
		 */
		public function flush(){
			if( $this->flush_rules )
				flush_rewrite_rules();
		}

		public function activate(){
			$this->flush_rules = TRUE; // we will need to refresh the rewrite rules for the custom post types
		}

		public function deactivate(){
			$this->flush_rules = TRUE; // refresh the rewrite rules for the custom post types which are no longuer loaded
		}

		public function imgURL( $file ){
			return $this->plugin_url() . "/assets/images/{$file}";
		}

		public function jsURL( $file ){
			return $this->plugin_url() . "/assets/js/{$file}";
		}
		public function jsPATH( $file ){
			return $this->plugin_path() . "/assets/js/{$file}";
		}

		public function cssURL( $file ){
			return $this->plugin_url() . "/assets/css/{$file}";
		}
		public function cssPATH( $file ){
			return $this->plugin_path() . "/assets/css/{$file}";
		}

		/**
		 * Get the plugin url.
		 *
		 * @access public
		 * @return string
		 */
		public function plugin_url() {
			if ( $this->plugin_url ) return $this->plugin_url;
			return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
		}

		/**
		 * Get the plugin path.
		 *
		 * @access public
		 * @return string
		 */
		public function plugin_path() {
			if ( $this->plugin_path ) return $this->plugin_path;
			return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		public function recent_tuts(){
			$recent_tuts = wp_get_recent_posts(array('post_type'=>'tutorial', 'numberposts'=>5 )); ?>
			<div class="widget">
				<h3><?php _e('Événements récents'); ?></h3>
				<ul>
					<?php foreach ($recent_tuts as $post): ?>
						<li><a href="<?php echo get_permalink($post["ID"]) ?>"><?php echo $post["post_title"]; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div><?php
		}

		public function list_cat(){
			$taxonomy = 'Événement_cat'; $tax_terms = get_terms($taxonomy); ?>
			<div class="widget">
				<h3><?php _e('Catégories'); ?></h3>
				<ul>
					<?php foreach ($tax_terms as $tax_term): ?>
						<li><a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><?php echo $tax_term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div><?php
		}

		public function list_tag(){
			$taxonomy = 'Événement_tag'; $tax_terms = get_terms($taxonomy); ?>
			<div class="widget">
				<h3><?php _e('Mots Clés'); ?></h3>
				<ul>
					<?php foreach ($tax_terms as $tax_term): ?>
						<li><a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><?php echo $tax_term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div><?php
		}
	}

	// Init Class and register in global scope
	$GLOBALS['CAAP_Events'] = new CAAP_Events();

	} // class_exists check
