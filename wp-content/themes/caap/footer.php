<?php
	global $gg_functions;
	$tagline   = get_field('tagline', 'options');
	$facebook  = get_field('facebook', 'options');
	$partners  = get_field('partners', 'options');
	$phone     = get_field('phone', 'options');
	$footer_bg = get_field('footer_banner', 'options');
?>
		</div>

		<footer class="footer">

			<?php if( $partners ): ?>
				<div class="partners-block">
					<div class="partners">
						<?php foreach ($partners as $partner ): ?>
							<div class="partner">
								<div class="img">
									<img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['alt']; ?>">
								</div>
								<div class="quotes">
									<span class="quote"><?php echo $partner['quote']; ?></span>
									<span class="name"><?php echo $partner['person'] ?></span>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="nav-wrap">
					<div class="partners-nav">
						<?php foreach ($partners as $partner ): ?>
							<div class="partner">
								<img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['alt']; ?>">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if( $footer_bg ): ?>
				<div class="footer-bg" style="background-image:url(<?php echo $footer_bg['url']; ?>);"></div>
			<?php endif; ?>
			<div class="brand">
				<div class="inner">
					<a class="logo" href="<?php echo site_url() ?>">
						<img src="<?php echo $gg_functions->imgURL('logo.png'); ?>" alt="<?php bloginfo('name') ?>" />
						<?php if( $tagline ): ?>
							<span class="tagline"><?php echo $tagline; ?></span>
						<?php endif; ?>
					</a>
					<div class="infos">
						<a class="link-button" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
						<a href="<?php echo $facebook; ?>" class="facebook"><i class="fa fa-facebook"></i></a>
					</div>
				</div>
			</div>
		</footer>

	</div>
	<?php wp_footer(); ?>
</body>
</html>
