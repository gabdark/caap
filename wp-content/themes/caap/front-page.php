<?php get_header(); ?>

<?php
	$about_title   = get_field('about_title');
	$about_text    = get_field('about_text');
	$about_firstb  = get_field('about_firstb');
	$about_firstl  = get_field('about_firstl');
	$about_secondb = get_field('about_secondb');
	$about_secondl = get_field('about_secondl');

	$img_events = get_field('img_events');

	$posts_args = array(
		'post_type'         =>'post',
		'posts_per_page'    => 3,
		'orderby'			=> 'date',
		'order'				=> 'DESC'
	);

	$events_args = array(
		'post_type'         =>'event',
		'posts_per_page'    => 2,
		'orderby'			=> 'date_event',
		'order'				=> 'ASC'
	);

	$latest_posts  = get_posts($posts_args);
	$latest_events = get_posts($events_args);

?>

<?php get_template_part('parts/home/slider'); ?>

<?php get_template_part('parts/blueband'); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
<section class="content container-fluid">

	<?php if( $about_title ): ?>
		<div class="about">
			<div class="inner">
				<div class="logo">
					<img src="<?php echo $gg_functions->imgURL('logo.png'); ?>" alt="<?php bloginfo('name') ?>" />
				</div>
				<div class="text">
					<h3><?php echo $about_title; ?></h3>
					<p><?php echo $about_text; ?></p>

					<div class="buttons">
						<?php if( $about_firstb ): ?>
							<a href="<?php echo $about_firstl; ?>" class="link-button"><?php echo $about_firstb; ?></a>
						<?php endif; ?>

						<?php if( $about_secondb ): ?>
							<a href="<?php echo $about_secondl; ?>" class="link-button blue"><?php echo $about_secondb; ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="newsfeed">
		<div class="posts">
			<?php if( $latest_posts ): ?>
				<h2><?php _e('Nouvelles du CAAP'); ?></h2>
				<div class="list-posts">
					<?php foreach ( $latest_posts  as $post ): setup_postdata( $post ); ?>
						<?php get_template_part('parts/list-post'); ?>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
				<a href="<?php echo get_post_type_archive_link( 'post' ); ?>" class="link-button blue"><?php _e('Voir toutes les nouvelles'); ?></a>
			<?php endif; ?>
		</div>
		<div class="events">
			<h2><?php _e('Nos événements'); ?></h2>
			<?php if( $latest_events ): ?>
				<div class="list-events">
					<?php foreach ( $latest_events  as $post ): setup_postdata( $post ); ?>
						<?php get_template_part('parts/list-event'); ?>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			<?php endif; ?>
			<img src="<?php echo $img_events['url']; ?>" alt="<?php echo $img_events['alt']; ?>">
		</div>
	</div>

	<div class="page-layouts">
		<?php if( have_rows('page_content') ) : while( have_rows('page_content') ) : the_row();

			get_template_part('parts/page/layout', get_row_layout() );

		endwhile; endif; ?>
	</div>

</section>
<?php endwhile; endif; ?>

<?php get_footer();
