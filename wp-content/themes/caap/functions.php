<?php

/**
 * GG Main Theme Functions
 *
 * @author Gabriel Gaudreau
 */

class GG_ThemeFunctions
{

	/**
	 * Theme Version, change to refresh css and js browser cache
	 * @var string
	 */
	public $version = '1.1.2';

	/**
	 * BOOT
	 * Set some constants and startup
	 */
	public function __construct(){
		define( 'GG_THEME_VERSION', $this->version );

		$this->maybe_update();
		$this->actions();
	}

	/**
	 * Check if theme version has changed and run update if it has
	 *
	 * @since 1.1.0
	 */
	public function maybe_update(){
		$installed = get_option( 'GG_ThemeFunctions_version', FALSE );

		if( ! $installed or version_compare($installed, GG_THEME_VERSION, '!=') ){
			$this->update();
			update_option( 'GG_ThemeFunctions_version', GG_THEME_VERSION );
		}
	}

	/**
	 * Runs if current theme version has changed, useful for "runonce" code
	 *
	 * @since 1.1.0
	 */
	public function update(){

	}

	/**
	 * Main Hooks
	 */
	public function actions(){
		// MAIN ACTIONS
		add_action( 'init', array($this, 'init') );
		add_action( 'after_setup_theme', array($this, 'after_setup_theme') );
		add_action( 'widgets_init', array($this, 'widgets_init') );
		add_action( 'wp_head', array($this, 'wp_head'), 1 );
		add_action( 'wp_enqueue_scripts', array($this, 'wp_enqueue_scripts') );

		// POST FILTERS
		add_filter( 'post_class', array($this, 'post_class') );
		add_filter( 'sanitize_file_name', array($this, 'sanitize_filename_on_upload'), 10 );
		add_filter( 'body_class', array($this, 'custom_taxonomy_in_body_class') );
		remove_filter( 'the_content', 'easy_image_gallery_append_to_content' );


		add_filter('acf/settings/save_json', array($this, 'GG_json_save_point'));

		// GRAVITY FORMS
		add_filter( 'gform_custom_merge_tags', array($this, 'custom_merge_tags'), 10, 4 );
		add_filter( 'gform_replace_merge_tags', array($this, 'replace_custom_merge_tags'), 10, 7 );
		add_filter( 'gform_submit_button', array($this, 'form_submit_button'), 10, 2 );
		add_filter( 'gform_notification', array( $this, 'notification_tpl' ), 10, 3 );
		add_filter( 'gform_email_background_color_label', array( $this, 'gform_email_background_color_label' ), 10, 3 );

		// MAKE SURE WPSEO METABOX IS AT THE BOTTOM
		add_filter( 'wpseo_metabox_prio', function(){ return 'low'; } );

		// PREVENT USELESS WARNING ABOUT PHP4 CONSTRUCTORS
		add_filter( 'deprecated_constructor_trigger_error', '__return_false' );

		$this->shortcodes();

	}

	public function init(){
		$this->menus();
		$this->sidebars();

		if( function_exists('acf_add_options_page') ) {

			acf_add_options_page(array(
				'page_title' 	=> 'CAAP',
				'menu_title'	=> 'CAAP',
				'menu_slug' 	=> 'fh-options',
				'redirect'		=> false
			));
		}

		if( $this->is_lab() ){
			$this->block_robots();
		}
	}

	/**
	 * Register theme menu locations
	 */
	public function menus(){
		register_nav_menus( array(
			'main-nav'   => __( 'Navigation principale', 'theme' ),
			'mobile-nav' => __( 'Navigation mobile', 'theme' ),
		));
	}

	/**
	 * Register theme sidebar locations for widgets
	 */
	public function sidebars(){
		register_sidebar(array(
			'id' 			=> 'blog-sidebar',
			'name' 			=> 'Sidebar blogue',
			'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<h3>',
			'after_title' 	=> '</h3>',
		));
	}

	/**
	 * Define new shortcodes
	 */
	public function shortcodes(){
		add_shortcode( 'email', array($this, 'sc_hide_email') );
		add_shortcode( 'custom-field', array($this, 'sc_custom_field') );
		add_shortcode( 'footer-lock', array($this, 'sc_footer_lock') );
		add_shortcode( 'googlemap', array($this, 'google_map') );
	}

	/**
	 * Hook run after theme is setup
	 */
	public function after_setup_theme(){
		// add support for post thumbnails
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'post-list-thumbnail', 520, 300, TRUE );
		add_image_size( 'single-post-image', 1170, 360, TRUE );
	}

	/**
	 * Hook run once widgets are ready
	 */
	public function widgets_init(){
		// prevent visual composer from checking for updates which creates errors
		wp_clear_scheduled_hook('wpb_check_for_update');
	}

	public function GG_json_save_point( $path ) {

	    // update path
	    $path = get_stylesheet_directory() . '/acf-json';


	    // return
	    return $path;

	}

	/**
	 * Output extra data to html's head
	 *
	 * @since revision 24: Removed maximum-scale=1.0, user-scalable=no
	 */
	public function wp_head(){
		global $is_IE;

		if( $is_IE ){ ?>
			<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
		<?php } ?>

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php

		$this->favicons();
	}

	/**
	 * Output favicon html in html's head
	 *
	 * @see wp_head()
	 */
	public function favicons(){
		?>
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->imgURL( 'favicons/apple-touch-icon.png' ) ?>">
		<link rel="icon" type="image/png" href="<?php echo $this->imgURL( 'favicons/favicon-32x32.png' ) ?>" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo $this->imgURL( 'favicons/favicon-16x16.png' ) ?>" sizes="16x16">
		<link rel="manifest" href="<?php echo $this->imgURL( 'favicons/manifest.json' ) ?>">
		<link rel="mask-icon" href="<?php echo $this->imgURL( 'favicons/safari-pinned-tab.svg' ) ?>" color="#e74d47">
		<meta name="theme-color" content="#ffffff">
		<?php
	}

	/**
	 * Queue JS and CSS files
	 */
	public function wp_enqueue_scripts(){
		// initialize dependencies and main js localization data
		$css_deps = array('style');
		$js_deps = array('jquery', 'underscore');
		$js_loc = array( 'ajaxurl' => admin_url('admin-ajax.php') );


		if( is_singular() && get_option( 'thread_comments' ) ){
			wp_enqueue_script( 'comment-reply' );
		}

		$this->enqueue_script( 'slick', 'slick.min.js' );
		$js_deps[] = 'slick';

		$this->enqueue_script( 'bootsrap-js', 'bootstrap.min.js' );
		$js_deps[] = 'bootsrap-js';

		$this->enqueue_style( 'main-css', 'style.css' );
		$this->enqueue_script( 'main-script', 'script.js', $js_deps );
		wp_localize_script( 'main-script', 'GG', $js_loc );

		$this->enqueue_style( 'ie-css', 'ie.css' );
		wp_style_add_data( 'ie-css', 'conditional', 'IE' );
	}

	/**
	 * Wrapper to enqueue typekit font
	 * @param  string $key the alphanumeric key for the js file
	 */
	public function enqueue_typekit( $key ){
		add_action( 'wp_head', array($this, 'theme_typekit_inline') );
		wp_enqueue_script( "theme_typekit", "//use.typekit.net/{$key}.js", array(), GG_THEME_VERSION, FALSE );
	}

	/**
	 * Start the enqueued typekit file
	 */
	public function theme_typekit_inline(){
		if( wp_script_is('theme_typekit', 'done') ){
			?><script type="text/javascript">try{Typekit.load();}catch(e){}</script><?php
		}
	}

	/**
	 * Enqueue a JS file
	 *
	 * @param  string  $handle    a unique identifier for the script
	 * @param  string  $file      url for the file
	 * @param  array   $deps      unique identifiers for scripts to load before this one
	 * @param  string  $ver       version of the file
	 * @param  boolean $in_footer do we load this file in the footer
	 *
	 * @uses wp_enqueue_script()
	 */
	public function enqueue_script( $handle, $file, $deps = array(), $ver = '', $in_footer = TRUE ){
		// if no version is used, use the last modified time of the file
		if( ! $ver ){
			$ver = @filemtime( $this->jsPATH($file) );
		}

		$src = $this->jsURL($file);
		if( ! file_exists($this->jsPATH( $file )) ){
			$src = $file;
		}

		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
	}

	/**
	 * Enqueue a CSS file
	 *
	 * @param  string $handle a unique identifier for the script
	 * @param  string $file   url for the file
	 * @param  array  $deps   unique identifiers for css files to load before this one
	 * @param  string $ver    version of the file
	 * @param  string $media  media attribute for the css file
	 *
	 * @uses wp_enqueue_style()
	 */
	public function enqueue_style( $handle, $file, $deps = array(), $ver = '', $media = 'all' ){
		// if no version is used, use the last modified time of the file
		if( ! $ver ){
			$ver = @filemtime( $this->cssPATH($file) );
		}

		$src = $this->cssURL($file);
		if( ! file_exists($this->cssPATH( $file )) ){
			$src = $file;
		}

		wp_enqueue_style( $handle, $src, $deps, $ver, $media );
	}

	/**
	 * Get the url for an image file
	 *
	 * @param  string $file file name for which to get the url
	 *
	 * @return string       url for the image
	 */
	public function imgURL( $file ){
		return get_stylesheet_directory_uri() . "/images/{$file}";
	}

	/**
	 * Get the url for a JS file
	 *
	 * @param  string $file file name for which to get the url
	 *
	 * @return string       url for the js file
	 */
	public function jsURL( $file ){
		return get_stylesheet_directory_uri() . "/js/{$file}";
	}
	/**
	 * Get the path for a JS file
	 *
	 * @param  string $file file name for which to get the path
	 *
	 * @return string       path for the js file
	 *
	 */
	public function jsPATH( $file ){
		return get_stylesheet_directory() . "/js/{$file}";
	}

	/**
	 * Get the url for a CSS file
	 *
	 * @param  string $file file name for which to get the url
	 *
	 * @return string       url for the css file
	 */
	public function cssURL( $file ){
		return get_stylesheet_directory_uri() . "/css/{$file}";
	}
	/**
	 * Get the path for a CSS file
	 *
	 * @param  string $file file name for which to get the path
	 *
	 * @return string       path for the css file
	 */
	public function cssPATH( $file ){
		return get_stylesheet_directory() . "/css/{$file}";
	}

	/**
	 * Wrapper for registering ajax calls
	 * @param  string $action the action name for the ajax
	 */
	public function register_ajax( $action ){
		add_action( "wp_ajax_$action", array($this, 'ajax_' . $action) );
		add_action( "wp_ajax_nopriv_$action", array($this, 'ajax_' . $action) );
	}

	/**
	 * Checks if current host is PAR Design DEV server
	 *
	 * @uses site_url()
	 *
	 * @return boolean is this the dev server?
	 */
	public function is_lab(){
		return ( stripos(site_url(), 'najomiestaging') !== FALSE or stripos(site_url(), 'webcervo') !== FALSE );
	}

	/**
	 * Writes the robots.txt file on the lab
	 *
	 * @see init()
	 * @see is_lab()
	 */
	public function block_robots(){
		$robots_path = ABSPATH . 'robots.txt';
		$robots_content = "User-agent: *\nDisallow: /";

		if( ! file_exists($robots_path) ){
			$fh = fopen( $robots_path, 'w' );

			if( $fh ){
				fwrite( $fh, $robots_content );
			}
		}
	}

	/**
	 * Add some classes to the post wrapper
	 *
	 * @param  string $classes original classes for the post wrapper
	 *
	 * @return string          modified classes for the post wrapper
	 */
	public function post_class( $classes ){
		$classes[] = 'cf'; // add float clearing class
		return $classes;
	}

	/**
	 * Make sure filenames are clean by removing accents
	 *
	 * @param  string $filename original filename
	 *
	 * @return string           sanitized filename
	 */
	public function sanitize_filename_on_upload( $filename ){
		return remove_accents( $filename );
	}

	public function get_excerpt($limit){
		$excerpt = get_the_content();
		$excerpt = preg_replace(" ([.*?])",'',$excerpt);
		$excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, $limit);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = $excerpt.'...';
		return $excerpt;
	}

	/**
	 * Output theme pagination for archives
	 */
	public function archive_pagination(){
		global $wp_query, $wp_rewrite;

		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

		$pagination = array(
			'base' => @add_query_arg( 'page','%#%' ),
			'format' => '',
			'total' => $wp_query->max_num_pages,
			'current' => $current,
					'show_all' => false,
					'end_size'     => 1,
					'mid_size'     => 2,
			'type' => 'list',
			'next_text' => '&gt;',
			'prev_text' => '&lt;'
		);

		if( $wp_rewrite->using_permalinks() )
			$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

		if( !empty($wp_query->query_vars['s']) )
			$pagination['add_args'] = array( 's' => get_query_var( 's' ) );

		echo paginate_links( $pagination );
	}

	/**
	 * Add taxonomy classes to body tag
	 *
	 * @param  string $classes original classes
	 *
	 * @return string          modified classes
	 */
	public function custom_taxonomy_in_body_class( $classes ){
		if( is_singular('product') or is_tax('category_product') ){
			$custom_terms = get_the_terms(0, 'category_product');

			if( $custom_terms ){
				foreach( $custom_terms as $custom_term ){
					if( $custom_term->parent ){
						$custom_term = get_term( $custom_term->parent, 'category_product' );
					}

					$classes[] = 'term-' . $custom_term->slug;
				}
			}
		}

		return $classes;
	}

	/**
	 * Custom tags for gravity forms
	 *
	 * @param  array	$merge_tags		original list of tags
	 * @param  int		$form_id		id of the current form
	 * @param  array	$fields			current form's fields
	 * @param  int		$element_id		id of the current element
	 *
	 * @return array             		modified list of tags
	 */
	public function custom_merge_tags($merge_tags, $form_id, $fields, $element_id){
		$merge_tags[] = array('label' => 'URL du site', 'tag' => '{site_url}');
		$merge_tags[] = array('label' => 'Nom de domaine du site', 'tag' => '{domain_name}');

		return $merge_tags;
	}

	/**
	 * Replace merge tags with their content
	 * @param  string $text       	The current text in which merge tags are being replaced
	 * @param  object $form       	Current form
	 * @param  object $entry      	Current entry
	 * @param  boolean $url_encode 	Whether or not to encode any URLs found in the replaced value
	 * @param  boolean $esc_html   	Whether or not to encode HTML found in the replaced value
	 * @param  boolean $nl2br      	Whether or not to convert newlines to break tags
	 * @param  string $format    	Default "html"; determines how the value should be formatted
	 * @return string             	The text with the merge tags replaced
	 */
	public function replace_custom_merge_tags($text, $form, $entry, $url_encode, $esc_html, $nl2br, $format) {
		$site_url    = site_url();
		$parse       = parse_url($site_url);

		return str_replace( array( '{site_url}', '{domain_name}' ), array( $site_url, $parse['host'] ), $text );
	}

	public function form_submit_button($button, $form) {
		return "<button class='button' id='gform_submit_button_{$form['id']}'><span>{$form['button']['text']}</span></button>";
	}

	public function notification_tpl( $notification, $form, $entry ){

		$title 		= $notification['subject'];
		$message 	= $notification['message'];

		$formatted_email = '';

		ob_start();

		include( get_stylesheet_directory() . '/inc/gf-email.php' );

		$formatted_email = ob_get_clean();

		$notification['message'] = $formatted_email;

		return $notification;
	}

	public function gform_email_background_color_label($ffffff, $field, $lead){
		$ffffff = 'f5f5f5';
		return $ffffff;
	}

	/**
	 * [email]email@example.com[/email]
	 */
	public function sc_hide_email( $atts , $content = null ){
		if( ! is_email($content) ) return;
		return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
	}

	/**
	 * [custom-field name="fieldname"]
	 */
	public function sc_custom_field( $atts ){
		global $post;
		return get_post_meta( $post->ID, $atts['name'], TRUE );
	}


	/**
	* [google-map]
	*/
	public function google_map( $atts ){

		// $this->enqueue_script( 'googleAPI', "https://maps.googleapis.com/maps/api/js?key={$this->googleAPIkey}&language=fr" );
		$this->enqueue_script( 'googlemap', 'googlemap.js', 'jquery' );

		$address = get_field('address', 'options');

		wp_localize_script( 'googlemap', 'GG_map',
			array(
				'icon' => $this->imgURL('marker.png'),
				'address' => __('<div><h4>CAAP</h4>'.$address.'</div>'),
			)
		);

		ob_start(); ?>

			<div class='gmap-wrapper'>
				<div id='map_canvas'></div>
			</div>
		<?php return ob_get_clean();
	}

	/**
	 * [footer-lock]
	 */
	public function sc_footer_lock( $atts ){
		ob_start(); ?>
		<a class="footer-lock" id="ptb_footer_lock" href="<?php bloginfo( 'wpurl' ); ?>/wp-admin"><i class="fa fa-lock"></i></a>
		<?php return ob_get_clean();
	}
}

$GLOBALS['gg_functions'] = new GG_ThemeFunctions();
