<?php global $gg_functions;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html id="top" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="dns-prefetch" href="//fonts.googleapis.com/">

	<title><?php wp_title(); ?></title>

	<?php wp_head(); ?>
</head>

<?php

$tagline  = get_field('tagline', 'options');
$facebook = get_field('facebook', 'options');
$phone    = get_field('phone', 'options');


?>

<body <?php body_class(); ?>>

	<div class="page-wrapper">

		<header class="header">

			<div class="brand">
				<div class="inner">
					<a class="logo" href="<?php echo site_url() ?>">
						<img src="<?php echo $gg_functions->imgURL('logo.png'); ?>" alt="<?php bloginfo('name') ?>" />
						<?php if( $tagline ): ?>
							<span class="tagline"><?php echo $tagline; ?></span>
						<?php endif; ?>
					</a>
					<div class="desktop infos">
						<a class="link-button" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
						<a href="<?php echo $facebook; ?>" class="facebook"><i class="fa fa-facebook"></i></a>
					</div>
				</div>
			</div>

			<?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => 'nav', 'container_class' => 'main-nav-wrap', 'fallback_cb' => 'false' )); ?>

			<?php get_template_part('parts/mobile-menu'); ?>
		</header>

		<div class="main">
