<?php get_header(); ?>

<?php
	global $gg_functions;
	$page_for_posts = get_option( 'page_for_posts' );
	$header_banner  = get_field('banner_img', $page_for_posts);
	$header_text    = get_field('subtitle', $page_for_posts);
	$show_blue      = get_field('show_blue_band', $page_for_posts);
	$blueband       = get_field('blue_band', 'options');

	$banner = $gg_functions->imgURL('default-banner.jpg');

	if( $header_banner ){
		$banner = $header_banner['url'];
	}
?>

<?php if( $banner ): ?>
    <section class="page-banner" style="background-image:url(<?php echo $banner; ?>)">
        <div class="inner">
            <h1><?php the_title(); ?></h1>
            <?php if( $header_text ): ?>
                <h2><?php echo $header_text; ?></h2>
            <?php endif; ?>
            <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>
        </div>
    </section>
<?php endif; ?>

<section class="content container-fluid">

	<div class="flex-grid">

		<div class="list-posts">

			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
				<?php get_template_part('parts/list-post'); ?>
			<?php endwhile; endif; ?>

			<div class="pagination"><?php $gg_functions->archive_pagination(); ?></div>

		</div>

		<aside class="sidebar">
			<form class="search" method="get" action="<?php echo site_url() ?>">
				<input class="input-seach" type="text" name="s" placeholder="<?php _e('Recherche ...'); ?>"/>
				<input type="hidden" name="post_type" value="post" />
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>

			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</aside>

	</div>

</section>

<?php get_footer();
