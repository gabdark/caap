jQuery(document).ready(function($) {

	$('#wpadminbar').addClass('Fixed');

	// Mobile menu
	$('.hamburger').click(function(){
		$(this).toggleClass('active');
		$('.mobile-menu').slideToggle(300);
		$('.mobile-menu').toggleClass('menu-active');
		return false;
	});

	$(window).on('scroll', _.throttle(function() {
		var st = $(this).scrollTop();

		if ( st >= 215 ) {
			$('.header').addClass('compact');
		} else {
			$('.header').removeClass('compact');
		}

	}, 100));


	// Parallax effect
	function parallax(div){
		var scrollTop = $(window).scrollTop();
		div.css('background-position', '0 '+ (scrollTop*0.5) + 'px');
	}
	if ($('.parallax').length){
		$(window).scroll(function(){
			parallax($('.parallax'));
		});
	}

	// FIX FOR _blank VULNERABILITY
	$('[target="_blank"]').attr( 'rel', 'noopener noreferrer' ).click(function(){

		var url = $(this).attr('href'),
			otherWindow = window.open();

		otherWindow.opener   = null;
		otherWindow.location = url;
	});

	// Slick Slider
	if ( $.fn.slick ) {

		var options_home = {
			accessibility: false,
			prevArrow: '<i class="fa fa-angle-left"></i>',
			nextArrow: '<i class="fa fa-angle-right"></i>',
			dots: true,
			pauseOnHover: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			draggable: true,
			swipe: true,
			autoplay: true,
			autoplaySpeed: 6000
		};

		var options_partners = {
			accessibility: false,
			dots: false,
			prevArrow: '<i class="fa fa-angle-left"></i>',
			nextArrow: '<i class="fa fa-angle-right"></i>',
			pauseOnHover: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 6000,
			asNavFor: '.partners-nav'
		};

		var nav = {
			slidesToShow: 6,
			slidesToScroll: 1,
			asNavFor: '.partners',
			dots: true,
			focusOnSelect: true
		};

		$('.home-slider').slick(options_home);
		$('.partners').slick(options_partners);
		$('.partners-nav').slick(nav);
	}

});
