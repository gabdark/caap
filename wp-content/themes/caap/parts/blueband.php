<?php $blueband = get_field('blue_band', 'options'); ?>
<?php if( $blueband ): ?>
<div class="blue-band">
	<div class="inner">
        <?php foreach ($blueband as $bloc ): ?>
            <a class="bloc" href="<?php echo $bloc['link'] ?>">
                <i style="background-image:url(<?php echo $bloc['icon']['url']; ?>);"></i>
                <div class="in">
                    <h3><?php echo $bloc['title']; ?></h3>
                    <p><?php echo $bloc['text']; ?></p>
                </div>
            </a>
        <?php endforeach; ?>
	</div>
</div>
<?php endif; ?>
