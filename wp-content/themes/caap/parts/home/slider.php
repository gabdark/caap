<?php $slider = get_field('slider'); ?>

<?php if( $slider ): ?>
    <div class="home-slider">
        <?php foreach ( $slider as $slide ): ?>
            <div class="slide" style="background-image:url(<?php echo $slide['img']['url']; ?>);">
                <?php if( $slide['link'] ): ?>
                    <a href="<?php echo $slide['link']; ?>"></a>
                <?php endif; ?>
                <div class="inner">
                    <?php if( $slide['title'] ): ?>
                        <h2><?php echo $slide['title']; ?></h2>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
