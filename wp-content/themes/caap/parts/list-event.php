<?php

$date = get_field('date_event', false, false);
$date = new DateTime($date);

?>
<article class="post">
    <div class="details">
        <h3><a href="<?php echo the_permalink(); ?>"><?php the_title() ?></a></h3>
        <p><span class="date"><?php echo $date->format('Y-m-d'); ?></span></p>
    </div>
</article>
