<?php
    global $gg_functions;

    $thumb = $gg_functions->imgURL('default-blog-thumbnail.jpg');
    $image_alt = __('CAAP - Nouvelles');

    if( has_post_thumbnail() ){
        $image_alt = get_post_meta( $image->id, '_wp_attachment_image_alt', true);
        $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(),'post-list-thumbnail', true);
        $thumb = $thumb[0];
    }
?>
<article class="post">
    <figure class="post-thumbnail">
        <a href="<?php echo the_permalink(); ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $image_alt; ?>"></a>
    </figure>
    <div class="details">
        <h3><a href="<?php echo the_permalink(); ?>"><?php the_title() ?></a></h3>
        <p><span class="date"><?php the_time('Y-m-d') ?> - </span><?php echo $gg_functions->get_excerpt(240); ?></p>
    </div>
</article>
