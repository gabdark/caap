<?php

$facebook = get_field('facebook', 'options');
$phone    = get_field('phone', 'options');
?>
<div class="mobile-nav-wrap">
    <div class="top-mobile">
        <button class="hamburger  mobile-nav-toggle menu-toggle" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>
    </div>
    <div class="mobile-menu">
        <ul class="top-m">
            <?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => false, 'items_wrap' => '%3$s' )); ?>
        </ul>
        <div class="mobile infos">
            <a class="link-button" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
            <a href="<?php echo $facebook; ?>" class="facebook"><i class="fa fa-facebook"></i></a>
        </div>
    </div>
</div>
