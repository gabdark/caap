<?php
    global $gg_functions;
    $header_banner = get_field('banner_img');
    $header_text   = get_field('subtitle');
    $show_blue     = get_field('show_blue_band');
    $blueband      = get_field('blue_band', 'options');

    $banner = $gg_functions->imgURL('default-banner.jpg');

    if( $header_banner ){
        $banner = $header_banner['url'];
    }
?>

<?php if( $banner ): ?>
    <section class="page-banner" style="background-image:url(<?php echo $banner; ?>)">
        <div class="inner">
            <h1><?php the_title(); ?></h1>
            <?php if( $header_text ): ?>
                <h2><?php echo $header_text; ?></h2>
            <?php endif; ?>
            <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>
        </div>
    </section>
<?php endif; ?>

<?php if( $show_blue == true ): ?>
    <div class="blue-band">
        <div class="inner">
            <?php if( $blueband ): ?>
                <?php foreach ($blueband as $bloc ): ?>
                    <a class="bloc" href="<?php echo $bloc['link'] ?>">
                        <i style="background-image:url(<?php echo $bloc['icon']['url']; ?>);"></i>
                        <div class="in">
                            <h3><?php echo $bloc['title']; ?></h3>
                            <p><?php echo $bloc['text']; ?></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
