<?php global $par_functions;

$layout = get_row_layout() . '_';
$anchor = get_sub_field(''.$layout.'anchor');
$img    = get_sub_field(''.$layout.'img');
$link   = get_sub_field(''.$layout.'link');

?>

<div id="<?php echo $anchor; ?>" class="<?php echo get_row_layout(); ?>">
    <a href="<?php echo $link; ?>">
        <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
    </a>
</div>
