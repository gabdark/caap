<?php get_header(); ?>

<?php
$date = get_field('date_event', false, false);
$date = new DateTime($date);

 ?>

<section class="content container-fluid">

	<div class="flex-grid">

		<div class="single-post">
			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

			<article class="post">

				<?php if ( has_post_thumbnail() ) : ?>
				<figure class="post-thumbnail">
					<?php the_post_thumbnail('single-post-image'); ?>
				</figure>
				<?php endif; ?>

				<h2><?php the_title(); ?></h2>

				<aside class="meta">
					<span class="loop-date"><?php echo $date->format('Y-m-d'); ?></span> /
					<span class="loop-cats"><?php the_category(', '); ?></span>
				</aside>

				<?php the_content(); ?>

			</article>

			<?php endwhile; endif; ?>

		</div>

		<aside class="sidebar">
			<form class="search" method="get" action="<?php echo site_url() ?>">
				<input class="input-seach" type="text" name="s" placeholder="<?php _e('Recherche ...'); ?>"/>
				<input type="hidden" name="post_type" value="post" />
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>

			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</aside>

	</div>

</section>

<?php get_footer();
