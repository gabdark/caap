<?php
    /* Template name: Contact */

    get_header();
    global $par_functions;

    $adress   = get_field('address', 'options');
    $phone    = get_field('telfax', 'options');
    $email    = get_field('email', 'options');

?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <?php
    	$bg 	   = get_field('page_bg');
        $title     = get_the_title();
    	$alt_title = get_field('alt_title');
    	$text 	   = get_field('intro_text');

        if( $alt_title ){
            $title = $alt_title;
        }
    ?>

    <?php echo do_shortcode('[googlemap]'); ?>
    <section class="content container-fluid">

        <div class="page-layouts">

            <div class="text-bloc">

                <div class="text-section">
                    <div class="left">
                        <div class="wrapper">
                            <div class="contact-info">
                                <h3><?php _e( 'Adresse')?></h3>
                                <p><?php echo $adress; ?></p>
                            </div>
                            <?php if( get_field('tel', 'options') ): ?>
                                <div class="contact-info">
                                    <h3><?php _e('Téléphone'); ?></h3>
                                    <p><?php echo get_field('tel', 'options'); ?> (<?php _e('appels locaux', 'constantin'); ?>)</p>
                                    <p><?php echo get_field('toll_free', 'options'); ?> ( <?php _e('partout au Canada', 'constantin'); ?>) </p>
                                </div>
                            <?php endif; ?>
                            <div class="contact-info">
                                <h3><?php _e('Courriel'); ?></h3>
                                <p><?php echo do_shortcode('[email]'.$email.'[/email]'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <h3><?php _e( 'Nous contacter')?></h3>
                        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
                    </div>
                </div>
            </div>

        </div>

    </section>

<?php endwhile; endif; ?>

<?php get_footer();
